## 1.1.6 (2024-05-23)

### changed (1 change)

- [Split Kubeadm / CNI plugins instructions + add parameter 'install_kubeadm'](alpetit/k8s-environment-recipe@9ad09390a666e3589f749e9caafc2307a55642b2)

## 1.1.5 (2024-04-22)

### changed (1 change)

- [Update containerd, kubeadm, and helm](alpetit/k8s-environment-recipe@28d56fa9f1a72c1d45806482238de5939e9fd6c4)

### fixed (1 change)

- [Remove default CNI configuration installed by crio](alpetit/k8s-environment-recipe@6795ed40a741b0b9339b2f585cf21f64285b3141)

## 1.1.3 (2024-03-06)

### fixed (1 change)

- [Diable auto-restart crio and containerd](alpetit/k8s-environment-recipe@74c79fe2914e8c55247614ec1a637ba01fa37f75)

## 1.1.2 (2024-03-06)

No changes.

## 1.1.1 (2024-03-06)

### fixed (1 change)

- [Set containerd in CI testing](alpetit/k8s-environment-recipe@f195f1187a81e23fa52724362275082e4c2e6f8a)

## 1.0.9 (2024-03-05)

### fixed (1 change)

- [Fix typo in cri-o recipe](alpetit/k8s-environment-recipe@3a7c06fe0b119cd116a680c9dff5ec04ffe43888)

### changed (2 changes)

- [Fix full version for cri-o and kubeadm](alpetit/k8s-environment-recipe@6393043d841d82804067f0c15d11b13dd5b5b360)
- [Move network settings to kubeadm recipe](alpetit/k8s-environment-recipe@592b87756f1994b709d87310e3bfebcdfcd53816)

### added (1 change)

- [Add recipe to install cri-o](alpetit/k8s-environment-recipe@ff07ab0f08c624ba8bc6af8d8f93ab68e0c0b046)

## 1.0.8 (2024-03-05)

### added (1 change)

- [Add python3-apt](alpetit/k8s-environment-recipe@ef48faf0977ccf763f540010d58d691beb144ad4)

### changed (1 change)

- [Change default configuration to have a 'shared' (not 'private') environment](alpetit/k8s-environment-recipe@6f50c24f5c6dd0f69ef9db75ad76e13add396df0)

## 1.0.7 (2024-02-29)

### added (1 change)

- [Add Grid5000 cache registry for containerd](alpetit/k8s-environment-recipe@b29b85e98c1be9673d37d5cdf759cc576d197a02)

## 1.0.6 (2024-02-21)

### changed (1 change)

- [Update debian11-min, Kubeadm, Helm and Containerd](alpetit/k8s-environment-recipe@44aadfacccc9d3061559be098a88d32acd4fe32c)

### added (1 change)

- [Add nfs-common to allow NFS PersistentVolume](alpetit/k8s-environment-recipe@4a47af904f04017a355f5fd201ffe3500812c653)

## 1.0.5 (2024-01-12)

### added (1 change)

- [Add python3-kubernetes to manage a k8s cluster via Python](alpetit/k8s-environment-recipe@b08fec7664e238f39b9451715d2d6d1e67e4920e)

## 1.0.4 (2023-12-13)

### changed (1 change)

- [Replace ntp by chrony to synchronize clock across nodes](alpetit/k8s-environment-recipe@d1125b59afdec5df19020bde5a8676a482b556b9)

## 1.0.3 (2023-12-12)

### added (1 change)

- [Add ntp to synchronize clock across nodes](alpetit/k8s-environment-recipe@cfe5ce625ccae2e6d0850d73e80e73d4fa8f7331)

## 1.0.2 (2023-11-28)

### fixed (1 change)

- [Fix several typos in gitlab-ci.yml](alpetit/k8s-environment-recipe@73d4078aa77f153147167f627f9957ea27f536fb)

## 1.0.1 (2023-11-14)

### added (2 changes)

- [Add to gitlab-ci.yml steps to automate the creation of a gitlab release and... (!4)](alpetit/k8s-environment-recipe@ba19d109e96f0265c4f9644ef080e63208ad021b)
- [Add helm installation recipe (!2)](alpetit/k8s-environment-recipe@fa3d1f663faf078da99d1231d39e7580903e225a)
